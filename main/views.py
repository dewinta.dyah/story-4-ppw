from django.shortcuts import render


def index(request):
    return render(request, 'main/index.html')

def aboutme(request):
    return render(request, 'main/aboutme.html')

def schedule(request):
    return render(request, 'main/schedule.html')

def create(request):
    return render(request, 'main/create.html')
