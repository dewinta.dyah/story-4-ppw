from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
class Schedule(models.Model):
    matkul = models.CharField(blank=False, max_length= 100)
    dosen = models.CharField(blank=False, max_length= 100)
    sks = models.IntegerField(blank=False)
    deskripsi = models.CharField(blank=False, max_length= 150)
    sem = models.CharField(blank=False, max_length= 150)
    kelas = models.CharField(blank=False, max_length= 150, help_text ='Masukkan tahun semester, e.g 2020/2021')
