from django.shortcuts import render, redirect
from .models import Schedule as jadwal
#from .models import Assg as tugas
from .forms import SchedForm

# Create your views here.
def Join(request):
    if(request.method == "POST"):
        form = SchedForm(request.POST)
        if (form.is_valid()):
            form2 = jadwal()
            form2.matkul = form.cleaned_data["matkul"]
            form2.dosen = form.cleaned_data["dosen"]
            form2.sks = form.cleaned_data["sks"]
            form2.deskripsi = form.cleaned_data["deskripsi"]
            form2.sem = form.cleaned_data["sem"]
            form2.kelas = form.cleaned_data["kelas"]
            form2.save()
        return redirect ("/jadwal")
    else:
        form2 = jadwal.objects.all()
        form = SchedForm()
        form_dictio ={
             "form2" : form2,
             "form": form
        }
        return render(request, 'jadwal.html', form_dictio)

def delete(request, pk):
    if (request.method == "POST"):
        form = SchedForm(request.POST)
        if(form.is_valid()):
            form2 = jadwal()
            form2.matkul = form.cleaned_data["matkul"]
            form2.dosen = form.cleaned_data["dosen"]
            form2.sks = form.cleaned_data["sks"]
            form2.deskripsi = form.cleaned_data["deskripsi"]
            form2.sem = form.cleaned_data["sem"]
            form2.kelas = form.cleaned_data["kelas"]
            form2.save()
        return redirect ("/jadwal")
    else:
        jadwal.objects.filter(pk = pk).delete()
        form = SchedForm()
        data = jadwal.objects.all()
        form_dictio ={
            "form2" : data,
            "form": form
        }
        return render(request, 'jadwal.html', form_dictio)

def detail(request, pk):
    matkul = jadwal.objects.get(pk=pk)
    detail_dictio= {
        "matkul" : matkul,

    }
    return render(request, 'detail.html', detail_dictio)
